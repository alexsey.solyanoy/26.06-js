window.addEventListener("DOMContentLoaded", () => {
  const keys = document.querySelector(".keys"),
      display = document.querySelector(".display input"),
      div = document.querySelector(".k")

  let calc = {
      firstNumber: "",
      secondNumber: "",
      sing: "",
      mrc: "",
      result: "",
      minus: "",
      plus: ""

  }

  let mrcC = 0;

  keys.addEventListener("click", (event) => {
      valiD(event.target.value);
  })

  const valiD = (patt) => {
      if (/^[0-9.]$/.test(patt) && calc.sing == "") {
          if (calc.result == "") {
              calc.firstNumber += patt;
              mrcC = 0
          } else if (calc.result !== "") {
              calc.result = ""
              calc.firstNumber = "";
              calc.firstNumber += patt;
              mrcC = 0
          }
          if (/\.{2,}/.test(calc.firstNumber)) {
              let repl = calc.firstNumber.replace(/\.{2,}/g, ".");
              calc.firstNumber = repl;
          } else if (/[0]{2,}\.+/.test(calc.firstNumber)) {
              let repla = calc.firstNumber.replace(/[0]{2,}\.+/g, "0.");
              calc.firstNumber = repla
          }
          display.value = calc.firstNumber
      } 
      else if (calc.firstNumber !== "" && calc.secondNumber == "" && /^[+/*-]$/.test(patt)) {
          calc.sing = patt;
          mrcC = 0
      }
       else if (calc.firstNumber !== "" && calc.sing !== "" && /^[0-9.]$/.test(patt)) {
          calc.secondNumber += patt;
          display.value = calc.secondNumber
          mrcC = 0
          if (/\.{2,}/.test(calc.secondNumber)) {
              let repl2 = calc.secondNumber.replace(/\.{2,}/g, ".");
              calc.secondNumber = repl2;
          } else if (/[0]{2,}\.+/.test(calc.secondNumber)) {
              let replase2 = calc.secondNumber.replace(/[0]{2,}\.+/g, "0.");
              calc.secondNumber = replase2
          }
      }
       else if (calc.secondNumber !== "" && calc.sing !== "" && /^[=+/*-]$/.test(patt)) {
          if (calc.sing == "+") {
              calc.result = (+calc.firstNumber) + (+calculator.secondNumber);
              calc.firstNumber = calc.result;
              calc.secondNumber = "";
              calc.sing = "";
              display.value = calc.result
          } else if (calc.sing == "-") {
              calc.result = calc.firstNumber - calc.secondNumber;
              calc.firstNumber = calc.result;
              calc.secondNumber = "";
              calc.sing = "";
              display.value = calc.result

          } else if (calc.sing == "*") {
              calc.result = calc.firstNumber * calc.secondNumber;
              calc.firstNumber = calc.result;
              calc.secondNumber = "";
              calc.sing = "";
              display.value = calc.result

          } else if (calc.sing == "/") {
              calc.result = calc.firstNumber / calc.secondNumber;
              calc.firstNumber = calc.result;
              calc.secondNumber = "";
              calc.arithmeticSign = "";
              display.value = calc.result

          }
      } 
       else if (patt == "m-" || patt === "m+") {
          if (patt === "m-") {
              calc.minus =  display.value * (-1) ;
              calc.firstNumber = "";
              calc.secondNumber = "";
              display.value = calc.minus
          } else if (patt == "m+") {
              calc.plus = display.value * (-1);
              calc.firstNumber = "";
              calc.secondNumber = "";
              display.value = calc.plus;
          }
          div.innerHTML = "m-"
      } else if (patt == "mrc") {
          if (mrcC == 0) {
              display.value = calc.mrc
              mrcC++
          } else if (mrcC == 1) {
              clean();
          }

      } else if (patt == "C") {
          clean();

      }
  }
  let clean = () => {
      calc.result = "";
      calc.firstNumber = "";
      calc.secondNumber = "";
      calc.sing = "";
      calc.mrc = ""
      display.value = ""
      div.innerHTML = ""
      mrcC = 0;
  }
})
