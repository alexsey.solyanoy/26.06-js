

document.getElementById('input1').onclick = () => {
  // инпут со строкой ввода
  const inp1 = document.createElement('input');
  inp1.setAttribute("type", "text")
  inp1.setAttribute("id", "inp1")
  document.body.append(inp1);

  // инпут кнопка
  const inp2 = document.createElement('input')
  inp2.setAttribute('type', 'button');
  inp2.setAttribute('id', 'inp2');
  inp2.value = "Нарисовать"
  document.body.append(inp2);
  if (inp2) {
    inp2.onclick = () => {
      for (let i = 0; i < 100; ++i) {
        let f = document.createElement('div');
        f.style.width = inp1.value + "px";                                                   
        f.style.backgroundColor = `hsl(${Math.floor(Math.random() * 360)}, 50%, 50%)`;
        f.style.height = inp1.value + "px";
        f.style.margin = '1rem';
        f.style.display = 'inline-block'
        f.style.borderRadius = '50%';
        inp2.after(f)


        let [...rand] = document.querySelectorAll("body>div");

        rand.forEach((el) => {
          el.onclick = () => {
            el.style.display = "none";
          } 
        })
      }
    }
  }
}