import React from 'react';
import  ReactDOM  from 'react-dom';

// основной элемент
const App = ()=>{
return (
    <div>
        <Month></Month>
        <br></br>
        <Days></Days>
        <br></br>
        <Zodiac></Zodiac>
    </div>
)
}
// Месяца
const Month = ()=>{
  return ( <div>
        <h1>Месяца</h1>
    <ul>
        <li>Январь</li>
        <li>Февраль</li>
        <li>Март</li>
        <li>Апрель</li>
        <li>Май</li>
        <li>Июнь</li>
        <li>Июль</li>
        <li>Август</li>
        <li>Сентябрь</li>
        <li>Октябрь</li>
        <li>Ноябрь</li>
        <li>Декабрь</li>
    </ul>
    </div>
    
    )
    
}

// Дни недели
const Days = ()=>{
 return (
    <div>
<h1>Дни недели</h1>
<ul>
    <li>Понедельник</li>
    <li>Вторник</li>
    <li>Среда</li>
    <li>Четверг</li>
    <li>Пятница</li>
    <li>Суббота</li>
    <li>Воскресение</li>
</ul>

    </div>
 )  
}

// Знаки Зодиака
const Zodiac = ()=>{
    return(
        <div>
<h1>Знаки Зодиака</h1>
<ul>
   <li>Овен</li>
   <li>Телец</li>
   <li>Близнец</li>
   <li>Рак</li>
   <li>Дева</li>
   <li>Весы</li>
   <li>Скорпион</li>
   <li>Стрелец</li>
   <li>Козерог</li>
   <li>Водолей</li>
   <li>Рыбы</li>
</ul>
        </div>
    )
}

ReactDOM.render(<App></App>,document.querySelector('#root'));